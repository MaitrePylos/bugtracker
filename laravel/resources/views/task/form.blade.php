<div class="box box-info padding-1">
    <div class="box-body">

        <div class="form-group">
            {{ Form::hidden('state', 'new') }}
        </div>
        <div class="form-group">
            {{ Form::label('description') }}
            {{ Form::textarea('description', $task->description, ['class' => 'form-control' . ($errors->has('description') ? ' is-invalid' : ''), 'placeholder' => '']) }}
            {!! $errors->first('description', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        @can('isCustomer')
            <div class="form-group">
                {{ Form::hidden('customer_id', Auth::user()->id) }}
            </div>
        @endcan
        @if (auth()->user()->can('isAdmin') || auth()->user()->can('isDebugger'))
            <div class="form-group">
                {{ Form::label('customer_id', 'Customer') }}
                {{ Form::select('customer_id', $users, $task->customer_id, ['class' => 'form-control' . ($errors->has('customer_id') ? ' is-invalid' : ''), 'placeholder' => '']) }}
                {!! $errors->first('customer_id', '<div class="invalid-feedback">:message</p>') !!}
            </div>
        @endif
        @can('isAdmin')
            <div class="form-group">
                {{ Form::label('debugger_id', 'Debugger') }}
                {{ Form::select('debugger_id', $users, $task->debugger_id, ['class' => 'form-control' . ($errors->has('debugger_id') ? ' is-invalid' : ''), 'placeholder' => '']) }}
                {!! $errors->first('debugger_id', '<div class="invalid-feedback">:message</p>') !!}
            </div>
        @endcan
        @can('isDebugger')
            <div class="form-group">
                {{ Form::label('debugger_id', 'Debugger') }}
                {{ Form::select('debugger_id', $users, Auth::user()->id, ['class' => 'form-control' . ($errors->has('debugger_id') ? ' is-invalid' : ''), 'placeholder' => '']) }}
                {!! $errors->first('debugger_id', '<div class="invalid-feedback">:message</p>') !!}
            </div>
        @endcan
        <div class="form-group">
            {{ Form::label('type_id', 'Type') }}
            {{ Form::select('type_id',$types, $task->type_id, ['class' => 'form-control' . ($errors->has('type_id') ? ' is-invalid' : ''), 'placeholder' => '']) }}
            {!! $errors->first('type_id', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('project_id', 'Project') }}
            {{ Form::select('project_id', $projects, $task->project_id, ['class' => 'form-control' . ($errors->has('project_id') ? ' is-invalid' : ''), 'placeholder' => '']) }}
            {!! $errors->first('project_id', '<div class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
