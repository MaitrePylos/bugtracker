@extends('layouts.app')

@section('template_title')
    Create Project
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Create Project</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('projects.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('project.form')

                        </form>
                        <br />
                        @can('isCustomer')
                            <a class="btn btn-info" href="{{ route('home') }}"> Back</a>
                        @endcan
                        @if (auth()->user()->can('isAdmin') || auth()->user()->can('isDebugger'))
                            <a class="btn btn-info" href="{{ route('projects.index') }}"> Back</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
