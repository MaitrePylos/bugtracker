<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['register' => false]);

Route::group(['middleware' => ['web', 'auth'], 'prefix' => ''], function () {
    Route::resource('tasks', 'TaskController');
    Route::resource('types', 'TypeController');
    Route::resource('users', 'UserController');
    Route::resource('projects', 'ProjectController');
    Route::get('contact', 'ContactController@create');
    Route::post('contact', 'ContactController@store');
});


Route::get('/home', 'HomeController@index')->name('home');
