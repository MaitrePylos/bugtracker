<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Project;
use Faker\Generator as Faker;
use App\Models\User;

$factory->define(Project::class, function (Faker $faker) {
    $users = User::all()->pluck('id')->toArray();

    return [
        'name' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'description' => $faker->text(),
        'customer_id' => $faker->randomElement($users),
        'manager_id' => $faker->randomElement($users),
    ];
});
