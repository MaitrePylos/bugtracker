@extends('layouts.app')
@section('template_title')
    {{ $task->name ?? 'Show Task' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <h3 class="card-title">Show Task</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <strong>State :</strong>
                            {{ $task->state }}
                        </div>
                        <div class="form-group">
                            <strong>Description :</strong>
                            {{ $task->description }}
                        </div>
                        <div class="form-group">
                            <strong>Customer :</strong>
                            {{ $task->user->name}}
                        </div>
                        <div class="form-group">
                            <strong>Debugger :</strong>
                            {{ $task->users->name ?? ''}}
                        </div>
                        <div class="form-group">
                            <strong>Type :</strong>
                            {{ $task->type->name }}
                        </div>
                        <div class="form-group">
                            <strong>Project :</strong>
                            {{ $task->project->name }}
                        </div>
                        @can('isCustomer')
                            <a class="btn btn-info" href="{{ route('home') }}"> Back</a>
                        @endcan
                        @if (auth()->user()->can('isAdmin') || auth()->user()->can('isDebugger'))
                            <a class="btn btn-info" href="{{ route('tasks.index') }}"> Back</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
