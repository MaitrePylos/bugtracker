<div class="row mt-5">
    <div class="col-sm-12">
        <div class="card border-left-primary border-primary shadow" style="width: 100%;">
            <div class="card-body">
                <div class="row ml-2 mr-2">
                    <div class="text-sm font-weight-bold text-primary text-uppercase mb-1">New Bugs</div>
                    <div class="table-responsive">
                        <table class="table table-striped table-borderless table-hover table-sm">
                            <thead class="thead-dark">
                            <tr class="d-flex">
                                <th class="col-1">No</th>
                                <th class="col-1">State</th>
                                <th class="col-3">Description</th>
                                <th class="col-2">Customer</th>
                                <th class="col-1">Type</th>
                                <th class="col-2">Project</th>
                                <th class="col-2">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($tasks as $task)
                                @if ($task->state == 'new')
                                    <tr class="d-flex">
                                        <th scope="row" class="col-1">{{ $task->id }}</th>
                                        <td class="col-1">{{ $task->state }}</td>
                                        <td class="col-3">{{ $task->description }}</td>
                                        <td class="col-2">{{ $task->user->name }}</td>
                                        <td class="col-1">{{ $task->type->name }}</td>
                                        <td class="col-2">{{ $task->project->name }}</td>
                                        <td class="col-2">
                                            <form action="{{ route('tasks.destroy',$task->id) }}" method="POST">
                                                <a class="btn btn-sm btn-primary " href="{{ route('tasks.show',$task->id) }}"><i class="fa fa-fw fa-eye"></i></a>
                                                <a class="btn btn-sm btn-success" href="{{ route('tasks.edit',$task->id) }}"><i class="fa fa-fw fa-edit"></i></a>
                                                @csrf
                                                @method('DELETE')
{{--                                                La suppression est temporairement indisponible--}}
{{--                                                <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i></button>--}}
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                {!! $tasks->links() !!}
            </div>
        </div>

    </div>
</div>
