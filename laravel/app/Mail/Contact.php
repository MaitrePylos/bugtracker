<?php

namespace App\Mail;
use App\Models\User;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Elements de contact
    * @var array
    */
    public $contact;
    public $users;
    public $task;

    /**
     * Create a new message instance.
    *
    * @return void
    */
    public function __construct(Array $contact, $task)
    {
        $this->contact = $contact;
        $this->users = Auth::user();
        $this->task = $task;
    }

    /**
     * Build the message.
    *
    * @return $this
    */
    public function build()
    {
        return $this->from('admin@bugtracker.com')
            ->view('emails.contact');
    }
}
