<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Task;
use Faker\Generator as Faker;
use App\Models\Project;
use App\Models\Type;
use App\Models\User;

$factory->define(Task::class, function (Faker $faker) {
    $state = ['new', 'in_progress', 'done'];
    $projects = Project::all()->pluck('id')->toArray();
    $types = Type::all()->pluck('id')->toArray();
    $users = User::all()->pluck('id')->toArray();
    return [
        'customer_id' => $faker->randomElement($users),
        'debugger_id' => $faker->randomElement($users),
        'type_id' => $faker->randomElement($types),
        'project_id' => $faker->randomElement($projects),
        'description' => $faker->text(),
        'state' => $faker->randomElement($state),
    ];
});
