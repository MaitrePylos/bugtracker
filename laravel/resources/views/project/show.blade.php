@extends('layouts.app')

@section('template_title')
    {{ $project->name ?? 'Show Project' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Project</span>
                        </div>
                    </div>

                    <div class="card-body">

                        <div class="form-group">
                            <strong>Name :</strong>
                            {{ $project->name }}
                        </div>
                        <div class="form-group">
                            <strong>Description :</strong>
                            {{ $project->description }}
                        </div>
                        <div class="form-group">
                            <strong>Customer :</strong>
                            {{ $project->users->name }}
                        </div>
                        <div class="form-group">
                            <strong>Manager :</strong>
                            {{ $project->users->name }}
                        </div>
                        @can('isCustomer')
                            <a class="btn btn-info" href="{{ route('home') }}"> Back</a>
                        @endcan
                        @if (auth()->user()->can('isAdmin') || auth()->user()->can('isDebugger'))
                            <a class="btn btn-info" href="{{ route('projects.index') }}"> Back</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
