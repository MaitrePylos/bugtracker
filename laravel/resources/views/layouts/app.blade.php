<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if (trim($__env->yieldContent('template_title'))) @yield('template_title') | @endif {{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="{{ asset('css/sb-admin-2.css')}}">
    <script type="text/javascript" src="{{ asset('js/sb-admin-2')}}"></script>

    <!-- Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
<!-- Page Wrapper -->
<div id="wrapper">
@auth
    <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
                <span>BugTracker</span>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link text-center" href="{{ route('home') }}">
                    <i class="fa fa-home fa-2x" aria-hidden="true"></i>&nbsp;HOME
                </a>
            </li>

        @if (auth()->user()->can('isAdmin') || auth()->user()->can('isDebugger'))

            <!-- Divider -->
            <hr class="sidebar-divider">


            <!-- Heading -->
            <div class="sidebar-heading text-center">
                Pages
            </div>

            <!-- Nav Item -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('projects.index') }}">
                    <i class="fa fa-flag fa-2x" aria-hidden="true"></i>&nbsp;Projects
                </a>
            </li>

            <!-- Nav Item -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('tasks.index') }}">
                    <i class="fa fa-tasks fa-2x" aria-hidden="true"></i>&nbsp;Tasks
                </a>
            </li>
        @endif

        @can('isAdmin')
            <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading text-center">
                    Config
                </div>

                <!-- Nav Item -->
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('users.index') }}">
                        <i class="fa fa-users fa-2x" aria-hidden="true"></i>&nbsp;Users
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('types.index') }}">
                        <i class="fa fa-bug fa-2x" aria-hidden="true"></i>&nbsp;Types
                    </a>
                </li>

        @endcan

        <!-- Divider -->
            <hr class="sidebar-divider">

            @can('isCustomer')
{{--            <span class="text-info text-center text-uppercase">{{ $userConnected->firstname }} {{ $userConnected->name }}</span>--}}
            @endcan

            <li class="nav-item">
                <a class="nav-link text-center" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
        <!-- End of Sidebar -->
    @endauth
    <div id="content-wrapper" class="d-flex flex-column">

        @yield('content')

        <footer>
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Webdevelopper Exam - Cursus 2019-2020</span>
                </div>
            </div>
        </footer>
    </div>
</div>
@include('sweetalert::alert')

<script>
    $('document').ready(function () {
        $('#table_users').dataTable();
        $('#table_projects').dataTable();
        $('#table_tasks').dataTable();
    })
</script>
</body>
</html>
