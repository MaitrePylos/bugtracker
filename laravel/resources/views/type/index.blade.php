@extends('layouts.app')

@section('template_title')
    Type
@endsection

@section('content')
    <div class="container-fluid">
        @can('isAdmin')
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Type') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('types.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>No</th>
										<th>Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($types as $type)
                                        <tr>
                                            <td>{{ ++$i }}</td>
											<td>{{ $type->name }}</td>
                                            <td>
                                                <form action="{{ route('types.destroy',$type->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('types.show',$type->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('types.edit',$type->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
{{--                                                    La suppression est temporairement indisponible--}}
{{--                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>--}}
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    {!! $types->links() !!}
                </div>
            </div>
        </div>
        @endcan
        @if (auth()->user()->can('isCustomer') || auth()->user()->can('isDebugger'))
            <div class="container mt-5 mb-5">
                <a class="btn btn-info" href="{{ route('home') }}"> Back HOME</a>
            </div>
        @endif
    </div>
@endsection
