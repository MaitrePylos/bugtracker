@extends('layouts.app')

@section('content')
    <!-- Main Content -->
    <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid mt-5">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            </div>

            <!-- Content Customer -->
            @can('isCustomer')
                @include('home_dashboard.customer')
            @endcan

            <!-- Content Debugger -->
            @can('isDebugger')
                @include('home_dashboard.debugger')
            @endcan

        <!-- Content Debugger -->
            @can('isAdmin')
                @include('home_dashboard.admin')
            @endcan

        </div>
    </div>


@endsection
