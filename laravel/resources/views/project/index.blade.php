@extends('layouts.app')

@section('template_title')
    Project
@endsection

@section('content')
    <div class="container-fluid">
        @if (auth()->user()->can('isAdmin') || auth()->user()->can('isDebugger'))
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Project') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('projects.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="table_projects" class="table table-striped table-hover table-sm">
                                <thead class="thead-dark">
                                    <tr class="d-flex">
										<th class="col-2">Name</th>
										<th class="col-5">Description</th>
										<th class="col-2">Customer</th>
										<th class="col-2">Manager</th>
                                        <th class="col-1">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($projects as $project)
                                        <tr class="d-flex">
											<td class="col-2">{{ $project->name }}</td>
											<td class="col-5">{{ $project->description }}</td>
											<td class="col-2">{{ $project->user->name }}</td>
											<td class="col-2">{{ $project->users->name ?? ''}}</td>
                                            <td class="col-2">
                                                <form action="{{ route('projects.destroy',$project->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('projects.show',$project->id) }}"><i class="fa fa-fw fa-eye"></i></a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('projects.edit',$project->id) }}"><i class="fa fa-fw fa-edit"></i></a>
                                                @can('isAdmin')
                                                    @csrf
                                                    @method('DELETE')
{{--                                                    La suppression est temporairement indisponible--}}
{{--                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i></button>--}}
                                                @endcan
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
            @can('isCustomer')
                <div class="container mt-5 mb-5">
                    <a class="btn btn-info" href="{{ route('home') }}"> Back HOME</a>
                </div>
            @endcan
        @endif
    </div>
@endsection
