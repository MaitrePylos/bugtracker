<?php

use Illuminate\Database\Seeder;
use App\Models\Type;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bug = new Type();
        $bug->name = 'Bug';
        $bug->save();

        $improvement = new Type();
        $improvement->name = 'Amélioration';
        $improvement->save();
    }
}
