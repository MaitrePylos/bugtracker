@extends('layouts.app')

@section('template_title')
    {{ $type->name ?? 'Show Type' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Type</span>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Name :</strong>
                            {{ $type->name }}
                        </div>
                        <a class="btn btn-info" href="{{ route('types.index') }}"> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
