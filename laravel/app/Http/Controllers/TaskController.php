<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use App\Models\Type;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

/**
 * Class TaskController
 * @package App\Http\Controllers
 */
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userConnected = Auth::user();

        $tasks = Task::all();
        $users = User::all()->pluck('name', 'id');
        $types = Type::all()->pluck('name', 'id');
        $projects = Project::all()->pluck('name', 'id');

        return view('task.index', compact('userConnected','tasks', 'users', 'types', 'projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userConnected = Auth::user();
        $userID = $userConnected->id;

        $task = new Task();
        $users = User::all()->pluck('name', 'id');
        $userConnectedName = User::all()->where('customer_id', $userID);
        $types = Type::all()->pluck('name', 'id');
        if ($userConnected->role == 'customer') {
            $projects = Project::all()->where('customer_id', $userID)->pluck('name', 'id');
        } else{
            $projects = Project::all()->pluck('name', 'id');
        }

        return view('task.create', compact('userConnectedName','task', 'users', 'types', 'projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Task::$rules);

        $task = Task::create($request->all());
        $state = $task->state;

        if ($state == 'new') {
            Mail::to('administrateur@chezmoi.com')
            ->send(new Contact($request->except('_token'), $task));
        }


        Alert::success('Task created', 'Successfully');

        return redirect()->route('home')
            ->with('success', 'Task created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);
        $users = User::all()->pluck('name', 'id');
        $types = Type::all()->pluck('name', 'id');
        $projects = Project::all()->pluck('name', 'id');

        return view('task.show', compact('task', 'users', 'types', 'projects'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        $users = User::all()->pluck('name', 'id');
        $types = Type::all()->pluck('name', 'id');
        $projects = Project::all()->pluck('name', 'id');

        return view('task.edit', compact('task', 'users', 'types', 'projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Task $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        request()->validate(Task::$rules);

        $task->update($request->all());

        Alert::success('Task created', 'Successfully');

        return redirect()->route('home');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $task = Task::find($id)->delete();

        return redirect()->route('tasks.index')
            ->with('success', 'Task deleted successfully');
    }
}
