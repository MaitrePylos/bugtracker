<div class="row">
    <div class="container d-flex justify-content-center">
        <a href="{{ route('tasks.create') }}">
            <button type="button" class="btn btn-primary">Déclarer un bug / proposer une amélioration</button>
        </a>
    </div>
</div>
<div class="row mt-5">
    <div class="container d-flex justify-content-center">
        <div class="card border-left-primary border-primary shadow" style="width: 100%;">
            <div class="card-body">
                <div class="row ml-2 mr-2">
                    <div class="text-sm font-weight-bold text-primary text-uppercase mb-1">Vos Bugs ou Améliorations
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-borderless table-hover table-sm">
                            <thead class="thead-dark">
                            <tr class="d-flex">
                                <th class="col-1">No</th>
                                <th class="col-2">State</th>
                                <th class="col-3">Description</th>
                                <th class="col-2">Debugger</th>
                                <th class="col-1">Type</th>
                                <th class="col-2">Project</th>
                                <th class="col-1">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($tasksOfCustomer as $task)
                                @if ($task->customer_id == $userConnected->id)
                                    <tr class="d-flex">
                                        <th scope="row" class="col-1">{{ $task->id }}</th>
                                        <td class="col-2">{{ $task->state }}</td>
                                        <td class="col-3">{{ $task->description }}</td>
                                        <td class="col-2">{{ $task->users->name ?? ''}}</td>
                                        <td class="col-1">{{ $task->type->name }}</td>
                                        <td class="col-2">{{ $task->project->name }}</td>
                                        <td class="col-1">
                                            <form action="{{ route('tasks.destroy',$task->id) }}" method="POST">
                                                <a class="btn btn-sm btn-primary "
                                                   href="{{ route('tasks.show',$task->id) }}"><i
                                                        class="fa fa-fw fa-eye"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-5">
    <div class="container d-flex justify-content-center">
        <div class="card border-left-primary border-primary shadow" style="width: 100%;">
            <div class="card-body">
                <div class="row ml-2 mr-2">
                    <div class="text-sm font-weight-bold text-primary text-uppercase mb-1">Vos Projets</div>
                    <div class="table-responsive">
                        <table class="table table-striped table-borderless table-hover table-sm">
                            <thead class="thead-dark">
                            <tr class="d-flex">
                                <th class="col-1">No</th>
                                <th class="col-3">Name</th>
                                <th class="col-5">Description</th>
                                <th class="col-2">Manager</th>
                                <th class="col-1">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($projectsOfUser as $project)
                                @if ($project->customer_id == $userConnected->id)
                                    <tr class="d-flex">
                                        <th scope="row" class="col-1">{{ $project->id }}</th>
                                        <td class="col-3">{{ $project->name }}</td>
                                        <td class="col-5">{{ $project->description }}</td>
                                        <td class="col-2">{{ $project->users->name }}</td>
                                        <td class="col-1">
                                            <form action="{{ route('projects.destroy',$project->id) }}" method="POST">
                                                <a class="btn btn-sm btn-primary "
                                                   href="{{ route('projects.show',$project->id) }}"><i
                                                        class="fa fa-fw fa-eye"></i></a>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
