<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_email = '@example.com';
        $users = ['admin', 'customer', 'debugger'];

        foreach ($users as $user) {
            $newUser = new User();
            $newUser->name = ucfirst($user);
            $newUser->firstname = ucfirst($user);
            $newUser->email =  $user . $test_email;
            $newUser->email_verified_at = now();
            $newUser->password = bcrypt($user);
            $newUser->remember_token = Str::random(10);
            $newUser->role = $user;
            $newUser->save();
        }

        factory(User::class, 100)->create();
    }
}
