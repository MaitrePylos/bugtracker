<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <style>
            body {
                font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
                /*background:*/
            }
            h2 {
                color:Teal;

            }
            ul li {
                margin-bottom: 10px;
            }
        </style>
    </head>
    <body>
        <h2>Report d'un bug</h2>
        <p>Un nouveau bug a été reporté.</p>

        <p>{{$users->firstname}} {{ $users->name }} a ajouté un nouveau bug.</p>
        <p>Description : {{ $contact['description'] }}</p>
        <a href={{ route('tasks.show', [$task->id]) }}>Lien vers la tâche</a>
    </body>
</html>
