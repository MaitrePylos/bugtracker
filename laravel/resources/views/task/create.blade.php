@extends('layouts.app')
@section('template_title')
    Create Task
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Create Task</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('tasks.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('task.form')

                        </form>
                        <br />
                        @can('isCustomer')
                            <a class="btn btn-info" href="{{ route('home') }}"> Back</a>
                        @endcan
                        @if (auth()->user()->can('isAdmin') || auth()->user()->can('isDebugger'))
                            <a class="btn btn-info" href="{{ route('tasks.index') }}"> Back</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
