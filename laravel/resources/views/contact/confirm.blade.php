@extends('layouts.app')

@section('content')
    <br>
    <div class="container">
        <div class="row card text-white bg-dark">
            <h4 class="card-header">Reporter un bug</h4>
            <div class="card-body">
                <p class="card-text">Merci pour votre message. Nous apprécions le temps que vous prenez pour améliorer avec nous votre expérience et notre programme.</p>
            </div>
        </div>
    </div>
@endsection
