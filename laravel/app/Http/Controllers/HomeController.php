<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userConnected = Auth::user();
        $userID = $userConnected->id;
        $tasks = Task::paginate();
        $tasksOfCustomer = Task::all()->where('customer_id', $userID);
        $tasksOfDebugger = Task::all()->where('debugger_id', $userID);
        $projectsOfUser = Project::all()->where('customer_id', $userID);

        return view('home', compact('userConnected','tasks', 'tasksOfCustomer', 'tasksOfDebugger', 'projectsOfUser'))
            ->with('i', (request()->input('page', 1) - 1) * $tasks->perPage());
    }
}
